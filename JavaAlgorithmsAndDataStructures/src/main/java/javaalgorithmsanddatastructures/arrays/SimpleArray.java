package javaalgorithmsanddatastructures.arrays;

import java.util.Arrays;

import javaalgorithmsanddatastructures.searching.Searching;
import javaalgorithmsanddatastructures.sorting.Sorting;

public class SimpleArray {

	private Integer[] array;
	private Integer size;
	private Integer numberOfElements;
	
	private Sorting sortingAlgorithm;
	private Searching searchingAlgorithm;

	public SimpleArray(Integer arraySize) {
		array = new Integer[arraySize]; 
		this.numberOfElements = 0;
		this.size = arraySize;
	}
	
	public Integer insert(Integer value) {
		array[numberOfElements] = value;
		numberOfElements++;
		
		return numberOfElements;
	}
	
	public Integer find(Integer value) {
		if (searchingAlgorithm != null) return searchingAlgorithm.find(this, value);
		
		return -1;
	}

	public Integer get(Integer index) {
		return array[index];
	}

	public Integer set(Integer index, Integer value) {
		array[index] = value;
		
		return array[index];
	}

	public Boolean delete(Integer value) {
		int index = find(value);
		
		if (index > 0) {
			array[index] = null;

			// смещение элементов массива влево
			vacuum(index);
			numberOfElements--;
			
			return true;
		}
		
		return false;
	}
	
	public void clearArray() {
		Arrays.stream(array).forEach(e -> e = null);
		numberOfElements = 0;
	}
	
	public String print() {
		StringBuilder output = new StringBuilder();
		
		Arrays.asList(array).stream()
			.forEach(value -> output.append(value + " "));
		
		return output.toString().trim();
	}
	
	public void sort() {
		if (sortingAlgorithm != null) sortingAlgorithm.sort(this);
	}
	
	/*
	 * Добавьте метод getMax(), который возвращает наибольшее значение ключа в
	 * массиве или ­–1, если массив пуст.
	 * */
	public Integer getMax() {
		return numberOfElements != 0 ? numberOfElements - 1 : -1;
	}
	
	/*
	 * Измените метод getMax() так, чтобы элемент с наибольшим ключом не
	 * только возвращался методом, но и удалялся из массива.
	 * */
	public Integer removeMax() {
		Integer max = getMax();
		
		if (max != -1) {
			delete(array[max]);
		}
		
		return max;
	}
	
	private void vacuum(int index) {
		/*
		 * Индекс определяющий последний элемент в массиве, после перемещения элементов
		 * влево.
		 * 
		 * Нужен для того чтобы последний сдвинутый элемент не хранился в последней и
		 * предпоследней ячейке, в которую он был перемещён.
		 */
		int lastMovedIndex = 0;
		for (int i = index; i < numberOfElements - 1; i++) {
			if (array[i + 1] != null) array[i] = array[i + 1];
			lastMovedIndex = i;
		}
		
		array[lastMovedIndex + 1] = null;
	}

	/*
	 * Добавьте метод merge(), объединяющий два упорядоченных исходных массива
	 * в один упорядоченный приемный массив.
	 * */
	public SimpleArray merge(SimpleArray addArray) {
		int length1 = array.length;
		int length2 = addArray.getSize();
		SimpleArray result = new SimpleArray(length1 + length2);
		
		for (int i = 0; i < length1; i++) {
			Integer v = array[i];
			
			if (v != null) result.insert(v);
		}
		
		for (int i = 0; i < length2; i++) {
			Integer v = addArray.get(i);
			
			if (v != null) result.insert(v);
		}
		
		return result;
	}
	
	/*
	 * Добавьте метод noDups().
	 * */
	public SimpleArray noDups() {
		SimpleArray result = new SimpleArray(this.size);
		boolean duplicate = false;
		
		for (int i = 0; i < this.size; i++) {
			for (int j = 0; j < result.getNumberOfElements() - 1; j++) {
				if (array[i] == result.get(j)) {
					duplicate = true;
					break;
				}
			}
			
			if (!duplicate) {
				result.insert(array[i]);
			} else {
				duplicate = false;
			}
		}
		
		return result;
	}
	
	public Integer getSize() {
		return size;
	}

	public Integer getNumberOfElements() {
		return numberOfElements;
	}

	public void setSortingAlgorithm(Sorting algorithm) {
		this.sortingAlgorithm = algorithm;
	}

	public void setSearchingAlgorithm(Searching searchingAlgorithm) {
		this.searchingAlgorithm = searchingAlgorithm;
	}
	
}
