package javaalgorithmsanddatastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javaalgorithmsanddatastructures.infixtopostfix.InfixToPostfix;

public class MainInfix {

	public static void main(String[] args) throws IOException {
		String input;
		String output;
		
		while (true) {
			System.out.print("Enter infix: ");
			System.out.flush();
			
			input = getString();
			
			if (input.equals("")) {
				break;
			}
			
			InfixToPostfix infixToPostfix = new InfixToPostfix(input);
			output = infixToPostfix.infixToPostfix();
			
			System.out.println("Postfix is " + output + "\n");
		}
	}

	public static String getString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		
		return s;
	}

}
