package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.lists.FirstLastList;

public class MainFirstLastList {

	public static void main(String[] args) {
		FirstLastList firstLastList = new FirstLastList();
		
		firstLastList.insertFirst(22L);
		firstLastList.insertFirst(44L);
		firstLastList.insertFirst(66L);
		
		firstLastList.insertLast(11L);
		firstLastList.insertLast(33L);
		firstLastList.insertLast(55L);
		
		firstLastList.display();
		
		firstLastList.deleteFirst();
		firstLastList.deleteFirst();
		
		firstLastList.display();
	}

}
