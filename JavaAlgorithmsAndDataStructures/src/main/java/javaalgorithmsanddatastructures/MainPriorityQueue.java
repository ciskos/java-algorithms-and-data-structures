package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.queues.PriorityQueue;

public class MainPriorityQueue {

	public static void main(String[] args) {
		PriorityQueue priorityQueue = new PriorityQueue(10);
		
		priorityQueue.insert(5);
		priorityQueue.insert(2);
		priorityQueue.insert(3);
		priorityQueue.insert(1);
		priorityQueue.insert(4);
		
		while (!priorityQueue.isEmpty()) {
			Integer item = priorityQueue.remove();
			System.out.println(item + " ");
		}
		
		System.out.println();
	}

}
