package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.lists.Node;
import javaalgorithmsanddatastructures.lists.SimpleLinkList;

public class MainLinkList {

	public static void main(String[] args) {
		SimpleLinkList linkList = new SimpleLinkList();
		
		linkList.insertFirst(22, 2.99);
		linkList.insertFirst(44, 4.99);
		linkList.insertFirst(66, 6.99);
		linkList.insertFirst(88, 8.99);
		
		linkList.display();
		
		while (!linkList.isEmpty()) {
			Node link = linkList.deleteFirst();
			System.out.print("Deleted ");
			link.display();
			System.out.println();
		}
		
		linkList.display();
		
		System.out.println("\n===\n");
		
		linkList.insertFirst(22, 2.99);
		linkList.insertFirst(44, 4.99);
		linkList.insertFirst(66, 6.99);
		linkList.insertFirst(88, 8.99);
		
		linkList.display();

		Node findNode = linkList.find(44);
		if (findNode != null) {
			System.out.println("Found node with key " + findNode.getIntData());
		} else {
			System.out.println("Can't find node");
		}

		Node deleteNode = linkList.delete(66);
		if (deleteNode != null) {
			System.out.println("Deleted node with key " + deleteNode.getIntData());
		} else {
			System.out.println("Can't delete node");
		}
		
		linkList.display();
	}

}
