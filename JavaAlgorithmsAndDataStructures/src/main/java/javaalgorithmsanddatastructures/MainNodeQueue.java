package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.lists.NodeQueue;

public class MainNodeQueue {

	public static void main(String[] args) {
		NodeQueue queue = new NodeQueue();
		
		queue.insert(20L);
		queue.insert(40L);
		
		queue.display();
		
		queue.insert(60L);
		queue.insert(80L);
		
		queue.display();
		
		queue.remove();
		queue.remove();
		
		queue.display();
	}

}
