package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.stacks.SimpleStack;

public class MainStack {

	public static void main(String[] args) {
		SimpleStack simpleStack = new SimpleStack(10);
		
		System.err.println("peek " + simpleStack.peek());
		System.out.println("isEmpty " + simpleStack.isEmpty());
		System.out.println("isFull " + simpleStack.isFull());
		System.out.println("getLength " + simpleStack.getSize());
		System.out.println("getTop " + simpleStack.getTop());
		System.out.println();
		
		simpleStack.push(1);
		simpleStack.push(2);
		simpleStack.push(3);
		
		System.err.println("peek " + simpleStack.peek());
		System.out.println("isEmpty " + simpleStack.isEmpty());
		System.out.println("isFull " + simpleStack.isFull());
		System.out.println("getLength " + simpleStack.getSize());
		System.out.println("getTop " + simpleStack.getTop());
		System.out.println();
		
		simpleStack.push(4);
		simpleStack.push(5);
		simpleStack.push(6);
		simpleStack.push(7);
		simpleStack.push(8);
		simpleStack.push(9);
		simpleStack.push(10);
		
		System.err.println("peek " + simpleStack.peek());
		System.out.println("isEmpty " + simpleStack.isEmpty());
		System.out.println("isFull " + simpleStack.isFull());
		System.out.println("getLength " + simpleStack.getSize());
		System.out.println("getTop " + simpleStack.getTop());
		System.out.println();

		System.out.println(simpleStack.push(11));
	}

}
