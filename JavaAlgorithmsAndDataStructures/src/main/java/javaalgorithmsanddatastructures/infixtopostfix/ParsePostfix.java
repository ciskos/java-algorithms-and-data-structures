package javaalgorithmsanddatastructures.infixtopostfix;

public class ParsePostfix {

	private PostfixStack postfixStack;
	private String input;
	
	public ParsePostfix(String input) {
		this.input = input;
	}
	
	public int parse() {
		postfixStack = new PostfixStack(20);
		char ch;
		int i;
		int num1;
		int num2;
		int interAns;
		
		for (i = 0; i < input.length(); i++) {
			ch = input.charAt(i);
			postfixStack.displayStack("" + ch + " ");
			
			if (ch >= '0' && ch <= '9') {
				postfixStack.push((int)(ch - '0'));
			} else {
				num2 = postfixStack.pop();
				num1 = postfixStack.pop();
				
				switch (ch) {
					case '+':
						interAns = num1 + num2;
						break;
					case '-':
						interAns = num1 - num2;
						break;
					case '*':
						interAns = num1 * num2;
						break;
					case '/':
						interAns = num1 / num2;
						break;
					default:
						interAns = 0;
						break;
				}
				
				postfixStack.push(interAns);
			}
		}
		
		interAns = postfixStack.pop();
		
		return interAns;
	}
	
}
