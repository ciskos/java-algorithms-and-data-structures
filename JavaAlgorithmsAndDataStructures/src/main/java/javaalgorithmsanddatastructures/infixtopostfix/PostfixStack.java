package javaalgorithmsanddatastructures.infixtopostfix;

public class PostfixStack {

	private Integer[] stack;
	private Integer size;
	private Integer top;
	
	public PostfixStack(Integer size) {
		stack = new Integer[size];
		this.size = size;
		this.top = -1;
	}
	
	public void push(Integer value) {
		stack[++top] = value;
	}
	
	public Integer pop() {
		return stack[top--];
	}
	
	public Integer peek() {
		return stack[top];
	}
	
	public Integer peekN(Integer index) {
		return stack[index];
	}
	
	public Boolean isEmpty() {
		return top == -1;
	}
	
	public boolean isFull() {
		return top == size - 1;
	}
	
	public Integer size() {
		return top + 1;
	}
	
	public void displayStack(String s) {
		System.out.print(s);
		System.out.println("Stack (bottom-->top): ");
		
		for (int i = 0; i < size(); i++) {
			System.out.print(peekN(i));
			System.out.print(" ");
		}
		
		System.out.println();
	}
	
}
