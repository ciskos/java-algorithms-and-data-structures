package javaalgorithmsanddatastructures.infixtopostfix;

public class InfixToPostfix {

	private InfixStack infixStack;
	private String input;
	private String output = "";

	public InfixToPostfix(String input) {
		this.input = input;
		Integer stackSize = input.length();
		infixStack = new InfixStack(stackSize);
	}

	public String infixToPostfix() {
		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			
			infixStack.displayStack("For " + ch + " ");
			
			switch (ch) {
				case '+':
				case '-':
					gotOperator(ch, 1);
					break;
				case '*':
				case '/':
					gotOperator(ch, 2);
					break;
				case '(':
					infixStack.push(ch);
					break;
				case ')':
					gotParentheses(ch);
					break;
				default:
					output = output + ch;
					break;
			}
		}
		
		while (!infixStack.isEmpty()) {
			infixStack.displayStack("While ");
			output = output + infixStack.pop();
		}
		
		infixStack.displayStack("End ");
		
		return output;
	}
	
	public void gotOperator(char operatorThis, int prec1) {
		while(!infixStack.isEmpty()) {
			char operatorTop = infixStack.pop();
			
			if (operatorTop == '(') {
				infixStack.push(operatorTop);
				break;
			} else {
				int prec2;
				
				if (operatorTop == '+' || operatorTop == '-') {
					prec2 = 1;
				} else {
					prec2 = 2;
				}
				
				if (prec2 < prec1) {
					infixStack.push(operatorTop);
					break;
				} else {
					output = output + operatorTop;
				}
			}
		}
		
		infixStack.push(operatorThis);
	}

	public void gotParentheses(char ch) {
		while (!infixStack.isEmpty()) {
			char chx = infixStack.pop();
			
			if (chx == '(') {
				break;
			} else {
				output = output + chx;
			}
		}
	}
	
}
