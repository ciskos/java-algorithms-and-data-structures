package javaalgorithmsanddatastructures.infixtopostfix;

public class InfixStack {

	private Character[] stack;
	private Integer size;
	private Integer top;
	
	public InfixStack(Integer size) {
		stack = new Character[size];
		this.size = size;
		this.top = -1;
	}
	
	public void push(Character value) {
		stack[++top] = value;
	}
	
	public Character pop() {
		return stack[top--];
	}
	
	public Character peek() {
		return stack[top];
	}
	
	public Character peekN(Integer index) {
		return stack[index];
	}
	
	public Boolean isEmpty() {
		return top == -1;
	}
	
	public boolean isFull() {
		return top == size - 1;
	}
	
	public Integer size() {
		return top + 1;
	}
	
	public void displayStack(String s) {
		System.out.print(s);
		System.out.println("Stack (bottom-->top): ");
		
		for (int i = 0; i < size(); i++) {
			System.out.print(peekN(i));
			System.out.print(" ");
		}
		
		System.out.println();
	}
	
}
