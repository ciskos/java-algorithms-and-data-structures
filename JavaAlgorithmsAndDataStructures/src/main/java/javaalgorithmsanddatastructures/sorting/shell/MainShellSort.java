package javaalgorithmsanddatastructures.sorting.shell;

import java.util.Random;

public class MainShellSort {

	public static void main(String[] args) {
		int maxSize = 10;
		ShellArray arr = new ShellArray(maxSize);
		Random random = new Random(42);
		
		for (int j = 0; j < maxSize; j++) {
			long n = random.nextLong(99);
			arr.insert(n);
		}
		
		arr.display();
		arr.shellSort();
		arr.display();
	}

}
