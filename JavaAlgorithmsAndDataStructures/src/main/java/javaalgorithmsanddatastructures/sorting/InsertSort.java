package javaalgorithmsanddatastructures.sorting;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public class InsertSort implements Sorting {

	@Override
	public void sort(SimpleArray array) {
		for (int outer = 1; outer < array.getNumberOfElements(); outer++) {
			Integer insertValue = array.get(outer);
			
			for (int inner = outer - 1; inner >= 0; inner--) {
				if (insertValue < array.get(inner)) {
					array.set(inner + 1, array.get(inner));
					array.set(inner, insertValue);
				} else {
					array.set(inner + 1, insertValue);
					break;
				}
			}
		}
	}

}
