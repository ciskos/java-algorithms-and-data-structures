package javaalgorithmsanddatastructures.sorting.quick;

import java.util.Random;

public class MainQuickSortMedian {

	public static void main(String[] args) {
		int maxSize = 16;
		ArrayIns arr = new ArrayIns(maxSize);
		Random random = new Random(42);

		for (int j = 0; j < maxSize; j++) {
			long n = random.nextLong(99);
			arr.insert(n);
		}
		
		arr.display();
		arr.quickSort();
		arr.display();
	}

}
