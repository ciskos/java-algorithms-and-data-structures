package javaalgorithmsanddatastructures.sorting;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public interface Sorting {

	void sort(SimpleArray array);
	
}
