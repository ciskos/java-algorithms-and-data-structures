package javaalgorithmsanddatastructures.sorting;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public class SelectSort implements Sorting {

	@Override
	public void sort(SimpleArray array) {
		
		for (int i = 0; i < array.getNumberOfElements(); i++) {
			int lowestValueIndex = i;
			
			for (int j = i; j < array.getNumberOfElements(); j++) {
				if (array.get(j) < array.get(lowestValueIndex)) {
					lowestValueIndex = j;
				}
			}
			
			int temp = array.get(i);
			array.set(i, array.get(lowestValueIndex));
			array.set(lowestValueIndex, temp);
		}
	}

}
