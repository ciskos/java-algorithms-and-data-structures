package javaalgorithmsanddatastructures.sorting;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public class BubbleSort implements Sorting {

	@Override
	public void sort(SimpleArray array) {
		for (int i = array.getNumberOfElements() - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (array.get(j) > array.get(j + 1)) {
					Integer temp = array.get(j + 1);
					array.set(j + 1, array.get(j));
					array.set(j, temp);
				}
			}
		}
	}

}
