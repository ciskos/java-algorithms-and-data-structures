package javaalgorithmsanddatastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javaalgorithmsanddatastructures.infixtopostfix.ParsePostfix;

public class MainPostfix {

	public static void main(String[] args) throws IOException {
		String input;
		int output;
		
		while (true) {
			System.out.print("Enter postfix: ");
			System.out.flush();
			
			input = getString();
			
			if (input.equals("")) {
				break;
			}
			
			ParsePostfix parsePostfix = new ParsePostfix(input);
			output = parsePostfix.parse();
			
			System.out.println("Evaluates to " + output);
		}
	}

	public static String getString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		
		return s;
	}
	
}
