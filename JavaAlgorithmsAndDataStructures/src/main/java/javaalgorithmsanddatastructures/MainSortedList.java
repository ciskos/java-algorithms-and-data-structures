package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.lists.SortedList;

public class MainSortedList {

	public static void main(String[] args) {
		SortedList list = new SortedList();
		
		list.insert(20L);
		list.insert(40L);
		
		list.display();
		
		list.insert(10L);
		list.insert(30L);
		list.insert(50L);
		
		list.display();
		
		list.remove();
		
		list.display();
	}

}
