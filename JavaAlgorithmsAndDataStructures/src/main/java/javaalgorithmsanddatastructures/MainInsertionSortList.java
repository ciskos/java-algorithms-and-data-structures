package javaalgorithmsanddatastructures;

import java.util.Random;

import javaalgorithmsanddatastructures.lists.InsertionSortList;
import javaalgorithmsanddatastructures.lists.InsertionSortNode;

public class MainInsertionSortList {

	public static void main(String[] args) {
		int size = 10;
		
		InsertionSortNode[] nodeArray = new InsertionSortNode[size];
		
		for (int i = 0; i < size; i++) {
			Random random = new Random();
			
			InsertionSortNode newNode = new InsertionSortNode(random.nextLong(100));
			
			nodeArray[i] = newNode;
		}
		
		System.out.print("Unsorted array: ");
		
		for (int i = 0; i < size; i++) {
			System.out.print(nodeArray[i].getLongData() + " ");
		}

		System.out.println();
		
		InsertionSortList insertionSortList = new InsertionSortList(nodeArray);

		for (int i = 0; i < size; i++) {
			nodeArray[i] = insertionSortList.remove();
		}
		
		System.out.print("Sorted Array: ");
		
		for (int i = 0; i < size; i++) {
			System.out.print(nodeArray[i].getLongData() + " ");
		}

		System.out.println();
		
	}

}
