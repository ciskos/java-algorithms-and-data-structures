package javaalgorithmsanddatastructures.searching;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public class BinarySearch implements Searching {

	@Override
	public Integer find(SimpleArray array, Integer searchValue) {
		if (array.getNumberOfElements() == 0) return -1;
		
		Integer leftIndex = 0;
		Integer rightIndex = array.getSize() - 1;

		while (true) {
			Integer centralIndex = (leftIndex + rightIndex) / 2;
			Integer centralValue = array.get(centralIndex);

			if (leftIndex > rightIndex) {
				return -1;
			} else if (searchValue == centralValue) {
				return centralIndex;
			} else if (searchValue > centralValue) {
				 leftIndex = centralIndex + 1;
			} else if (searchValue < centralValue) {
				 rightIndex = centralIndex - 1;
			}
		}
	}

}
