package javaalgorithmsanddatastructures.searching;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public interface Searching {

	Integer find(SimpleArray array, Integer searchValue);
	
}
