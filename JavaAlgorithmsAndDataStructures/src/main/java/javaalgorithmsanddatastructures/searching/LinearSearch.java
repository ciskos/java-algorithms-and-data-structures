package javaalgorithmsanddatastructures.searching;

import javaalgorithmsanddatastructures.arrays.SimpleArray;

public class LinearSearch implements Searching {

	@Override
	public Integer find(SimpleArray array, Integer searchValue) {
		for (int i = 0; i < array.getNumberOfElements(); i++) {
			if (array.get(i) == searchValue) return i;
		}
		
		return -1;
	}

}
