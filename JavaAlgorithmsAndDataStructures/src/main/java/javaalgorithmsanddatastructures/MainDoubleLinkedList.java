package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.lists.doublelinked.DoubleLinkedList;

public class MainDoubleLinkedList {

	public static void main(String[] args) {
		DoubleLinkedList list = new DoubleLinkedList();
		
		list.insertFirst(22L);
		list.insertFirst(44L);
		list.insertFirst(66L);
		
		list.insertLast(11L);
		list.insertLast(33L);
		list.insertLast(55L);
		
		list.displayForward();
		list.displayBackward();
		
		list.deleteFirst();
		list.deleteLast();
		list.deleteKey(11L);
		
		list.displayForward();
		
		list.insertAfter(22L, 77L);
		list.insertAfter(33L, 88L);
		
		list.displayForward();
	}

}
