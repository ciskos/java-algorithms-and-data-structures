package javaalgorithmsanddatastructures.queues;

public class SimpleQueue {

	private Integer[] queue;
	private Integer size;
	private Integer numberOfElements;
	private Integer head;
	private Integer tail;
	
	public SimpleQueue(Integer size) {
		this.size = size;
		this.queue = new Integer[size];
		this.numberOfElements = 0;
		this.head = 0;
		this.tail = -1;
	}
	
	/*
	 *  insert
	 *  @return 
	 *  */
	public Integer insert(Integer value) {
		numberOfElements++;
		if (tail == size - 1) tail = -1;
		queue[++tail] = value;
		
		return head;
	}
	
	// remove
	public Integer remove() {
		numberOfElements--;
		
		Integer removed = queue[head++];
		
		if (head == size) head = 0;
		
		return removed;
	}
	
	// peekHead
	public Integer peekHead() {
		return queue[head];
	}
	
	// peekTail
	public Integer peekTail() {
		return tail > 0 ? queue[tail] : tail;
	}
	
	// isFull
	public Boolean isFull() {
		return numberOfElements == size;
	}
	
	// isEmpty
	public Boolean isEmpty() {
		return numberOfElements == 0;
	}

	// size
	public Integer getSize() {
		return size;
	}

	public Integer getNumberOfElements() {
		return numberOfElements;
	}
	
}
