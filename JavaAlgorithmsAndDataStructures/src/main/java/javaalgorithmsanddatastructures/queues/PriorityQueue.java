package javaalgorithmsanddatastructures.queues;

public class PriorityQueue {

	private Integer[] priorityQueue;
	private Integer size;
	private Integer numberOfElements;
	
	public PriorityQueue(Integer size) {
		this.priorityQueue = new Integer[size];
		this.size = size;
		this.numberOfElements = 0;
	}
	
	public void insert(Integer value) {
		int i;
		
		if (numberOfElements == 0) {
			priorityQueue[numberOfElements++] = value;
		} else {
			for (i = numberOfElements - 1; i >= 0; i--) {
				if (value > priorityQueue[i]) {
					priorityQueue[i + 1] = priorityQueue[i];
				} else {
					break;
				}
			}
			
			priorityQueue[i + 1] = value;
			numberOfElements++;
		}
	}
	
	public Integer remove() {
		return priorityQueue[--numberOfElements];
	}
	
	public Integer peekMin() {
		return priorityQueue[numberOfElements - 1];
	}
	
	public Boolean isEmpty() {
		return numberOfElements == 0;
	}
	
	public Boolean isFull() {
		return numberOfElements == size;
	}
	
}
