package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.queues.SimpleQueue;

public class MainQueue {

	public static void main(String[] args) {
		SimpleQueue queue = new SimpleQueue(10);
		
		System.out.println("number of elements " + queue.getNumberOfElements());
		System.out.println("size " + queue.getSize());
		System.out.println("head " + queue.peekHead());
		System.out.println("tail " + queue.peekTail());
		System.out.println("is empty " + queue.isEmpty());
		System.out.println("is full " + queue.isFull());
		System.out.println();
		
		queue.insert(1);
		queue.insert(2);
		queue.insert(3);
		
		System.out.println("number of elements " + queue.getNumberOfElements());
		System.out.println("size " + queue.getSize());
		System.out.println("head " + queue.peekHead());
		System.out.println("tail " + queue.peekTail());
		System.out.println("is empty " + queue.isEmpty());
		System.out.println("is full " + queue.isFull());
		System.out.println();
		
		queue.insert(4);
		queue.insert(5);
		queue.insert(6);
		queue.insert(7);
		queue.insert(8);
		queue.insert(9);
		queue.insert(10);
		
		System.out.println("number of elements " + queue.getNumberOfElements());
		System.out.println("size " + queue.getSize());
		System.out.println("head " + queue.peekHead());
		System.out.println("tail " + queue.peekTail());
		System.out.println("is empty " + queue.isEmpty());
		System.out.println("is full " + queue.isFull());
		System.out.println();
		
		queue.insert(11);
		
		System.out.println("number of elements " + queue.getNumberOfElements());
		System.out.println("size " + queue.getSize());
		System.out.println("head " + queue.peekHead());
		System.out.println("tail " + queue.peekTail());
		System.out.println("is empty " + queue.isEmpty());
		System.out.println("is full " + queue.isFull());
		System.out.println();
		
		queue.insert(12);
		
		System.out.println("number of elements " + queue.getNumberOfElements());
		System.out.println("size " + queue.getSize());
		System.out.println("head " + queue.peekHead());
		System.out.println("tail " + queue.peekTail());
		System.out.println("is empty " + queue.isEmpty());
		System.out.println("is full " + queue.isFull());
		System.out.println();
		
		queue.insert(13);
		queue.insert(14);
		queue.insert(15);
		queue.insert(16);
//		queue.remove();
//		queue.remove();
//		queue.remove();
		
		System.out.println("number of elements " + queue.getNumberOfElements());
		System.out.println("size " + queue.getSize());
		System.out.println("head " + queue.peekHead());
		System.out.println("tail " + queue.peekTail());
		System.out.println("is empty " + queue.isEmpty());
		System.out.println("is full " + queue.isFull());
		System.out.println();
	}

}
