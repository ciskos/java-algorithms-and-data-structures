package javaalgorithmsanddatastructures;

import javaalgorithmsanddatastructures.stacks.NodeStack;

public class MainStackNodeList {

	public static void main(String[] args) {
		NodeStack nodeStack = new NodeStack();
		
		nodeStack.push(20L);
		nodeStack.push(40L);
		
		nodeStack.display();
		
		nodeStack.push(60L);
		nodeStack.push(80L);
		
		nodeStack.display();
		
		nodeStack.pop();
		nodeStack.pop();
		
		nodeStack.display();
	}

}
