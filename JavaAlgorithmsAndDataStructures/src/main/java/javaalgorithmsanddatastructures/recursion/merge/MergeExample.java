package javaalgorithmsanddatastructures.recursion.merge;

public class MergeExample {

	public static void main(String[] args) {
		int[] arrayA = { 23, 47, 81, 95 };
		int[] arrayB = { 7, 14, 39, 55, 62, 74 };
		int[] arrayC = new int[10];

		merge(arrayA, arrayB, arrayC);
		display(arrayC);
	}

	public static void merge(int[] arrayA, int[] arrayB, int[] arrayC) {
		int aPointer = 0;
		int bPointer = 0;
		int cPointer = 0;
		int sizeA = arrayA.length;
		int sizeB = arrayB.length;

		while (aPointer < sizeA && bPointer < sizeB) { // Ни один из массивов не пуст
			if (arrayA[aPointer] < arrayB[bPointer]) {
				arrayC[cPointer++] = arrayA[aPointer++];
			} else {
				arrayC[cPointer++] = arrayB[bPointer++];
			}
		}

		while (aPointer < sizeA) { // Массив arrayB пуст,
			arrayC[cPointer++] = arrayA[aPointer++]; // в arrayA остались элементы
		}

		while (bPointer < sizeB) { // Массив arrayA пуст,
			arrayC[cPointer++] = arrayB[bPointer++]; // в arrayB остались элементы
		}
	}

	public static void display(int[] arrayC) {
		for (int j = 0; j < arrayC.length; j++) {
			System.out.print(arrayC[j] + " ");
		}
		
		System.out.println("");
	}

}
