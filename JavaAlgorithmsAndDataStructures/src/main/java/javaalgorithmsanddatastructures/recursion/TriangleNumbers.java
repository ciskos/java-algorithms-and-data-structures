package javaalgorithmsanddatastructures.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TriangleNumbers {

	static int number;
	
	public static void main(String[] args) throws IOException {
		System.out.print("Введите число: ");
		
		number = getInt();
		
		int result = triangle(number);
		
		System.out.println("Треугольное число = " + result);
		
		int alternativeResult = alternativeTriangle(number);
		
		System.out.println("Треугольное число = " + alternativeResult);
	}

	public static int triangle(int n) {
		return n == 1 ? 1 : n + triangle(n - 1);
	}
	
	public static int alternativeTriangle(int n) {
		System.out.println("Вычисление : n = " + n);
		
		if (n == 1) {
			System.out.println("Возврат 1");
			return 1;
		} else {
			int temp = n + alternativeTriangle(n - 1);
			System.out.println("Возврат " + temp);
			return temp;
		}
	}
	
	public static String getString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		String s = br.readLine();
		
		return s;
	}
	
	public static int getInt() throws IOException {
		String s = getString();
		
		return Integer.parseInt(s);
	}
	
}
