package javaalgorithmsanddatastructures.recursion;

public class Factorial {

	public static void main(String[] args) {
		int i = 5;
		
		System.out.println(factorial(i));
	}

	public static int factorial(int n) {
		return n == 0 ? 1 : n * factorial(n - 1);
	}
	
}
