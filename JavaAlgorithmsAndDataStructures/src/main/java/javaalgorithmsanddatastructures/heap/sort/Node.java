package javaalgorithmsanddatastructures.heap.sort;

class Node {
	
	private int iData; // data item (key)

	public Node(int key) // constructor
	{
		iData = key;
	}

	public int getKey() {
		return iData;
	}

}