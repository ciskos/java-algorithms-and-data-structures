package javaalgorithmsanddatastructures;

import java.util.Random;
import java.util.stream.IntStream;

import javaalgorithmsanddatastructures.arrays.SimpleArray;
import javaalgorithmsanddatastructures.searching.BinarySearch;
import javaalgorithmsanddatastructures.searching.LinearSearch;
import javaalgorithmsanddatastructures.searching.Searching;
import javaalgorithmsanddatastructures.sorting.BubbleSort;
import javaalgorithmsanddatastructures.sorting.InsertSort;
import javaalgorithmsanddatastructures.sorting.SelectSort;
import javaalgorithmsanddatastructures.sorting.Sorting;

public class MainArray {

	public static void main(String[] args) {
		SimpleArray simpleArray = new SimpleArray(10);
		Sorting bubbleSort = new BubbleSort();
		Sorting selectSort = new SelectSort();
		Sorting insertSort = new InsertSort();
		Searching linearSearch = new LinearSearch();
		Searching binarySearch = new BinarySearch();
		
		fillArray(simpleArray);

		System.out.println("\n===Printing");
		System.out.println("===> " + simpleArray.print());
		
		System.out.println("\n===Linear Searching - Success");
		simpleArray.setSearchingAlgorithm(linearSearch);
		System.out.println("find " + simpleArray.find(15));
		
		System.out.println("\n===Linear Searching - Fail");
		System.out.println("find " + simpleArray.find(11));
		
		System.out.println("\n===Binary Searching - Success");
		simpleArray.setSearchingAlgorithm(binarySearch);
		System.out.println("find " + simpleArray.find(15));
		
		System.out.println("\n===Binary Searching - Fail");
		simpleArray.setSearchingAlgorithm(binarySearch);
		System.out.println("find " + simpleArray.find(11));
		
		System.out.println("\n===Deleting");
		System.out.println("delete " + simpleArray.delete(91));
		System.out.println("\n===> " + simpleArray.print());
		
		System.out.println("\n===Deleting");
		System.out.println("delete " + simpleArray.delete(11));
		System.out.println("\n===> " + simpleArray.print());

		System.out.println("\n===Bubble Sorting");
		simpleArray.setSortingAlgorithm(bubbleSort);
		System.out.println("\nbefore sort ===> " + simpleArray.print());
		simpleArray.sort();
		System.out.println("\nafter sort ===> " + simpleArray.print());
		
		System.out.println("\n===Get maximum index");
		System.out.println(simpleArray.getMax());
		System.out.println("\n===> " + simpleArray.print());
		
		System.out.println("\n===Remove element with maximum index");
		System.out.println(simpleArray.removeMax());
		System.out.println("\n===> " + simpleArray.print());
		
		System.out.println("\n===Merge two arrays");
		SimpleArray array1 = new SimpleArray(10);
		SimpleArray array2 = new SimpleArray(20);
		
		fillArray(array1);
		fillArray(array2);
		
		SimpleArray mergedArrays = array1.merge(array2);
		System.out.println("\n===> " + mergedArrays.print());
		
		System.out.println("\n===Remove duplicates");
		
		System.out.println("\n===> " + (mergedArrays.noDups()).print());
		
		System.out.println("\n===Select sorting");
		simpleArray.clearArray();
		fillArray(simpleArray);
		System.out.println("\nbefore sort ===> " + simpleArray.print());
		simpleArray.setSortingAlgorithm(selectSort);
		simpleArray.sort();
		System.out.println("\nafter sort ===> " + simpleArray.print());
		
		System.out.println("\n===Insert sorting");
		simpleArray.clearArray();
		fillArray(simpleArray);
		System.out.println("\nbefore sort ===> " + simpleArray.print());
		simpleArray.setSortingAlgorithm(insertSort);
		simpleArray.sort();
		System.out.println("\nafter sort ===> " + simpleArray.print());
	}

	private static void fillArray(SimpleArray array) {
		Random random = new Random(100);
		
		IntStream.range(0, array.getSize())
			.forEach(i -> array.insert(random.nextInt(0, 100)));
	}
}
