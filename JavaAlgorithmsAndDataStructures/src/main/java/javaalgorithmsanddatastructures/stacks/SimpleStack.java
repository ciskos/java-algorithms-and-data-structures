package javaalgorithmsanddatastructures.stacks;

public class SimpleStack {

	private Integer[] stack;
	private Integer size;
	private Integer numberOfElements;
	private Integer top;
	
	public SimpleStack(Integer size) {
		this.stack = new Integer[size];
		this.size = size;
		this.numberOfElements = 0;
		this.top = -1;
	}

	// push
	public Integer push(Integer value) {
		if (isFull()) return null;
		
		stack[++top] = value;
		numberOfElements++;
		
		return top;
	}
	
	// pop
	public Integer pop() {
		numberOfElements--;
		return stack[top--];
	}
	
	// peek
	public Integer peek() {
		return top > -1 ? stack[top] : top;
	}
	
	// isEmpty
	public Boolean isEmpty() {
		return numberOfElements == 0;
	}
	
	// isFull
	public Boolean isFull() {
		return numberOfElements == size;
	}
	
	public Integer getSize() {
		return size;
	}

	public Integer getTop() {
		return top;
	}
	
}
