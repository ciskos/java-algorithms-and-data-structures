package javaalgorithmsanddatastructures.stacks;

public class StackNodeList {

	private StackNode first;
	
	public Boolean isEmpty() {
		return first == null;
	}
	
	public void insertFirst(Long longData) {
		StackNode newNode = new StackNode(longData);
		
		newNode.setNext(first);
		first = newNode;
	}
	
	public Long deleteFirst() {
		StackNode temp = first;
		first = first.getNext();
		
		return temp.getLongData();
	}
	
	public void display() {
		StackNode current = first;
		
		while (current != null) {
			current.display();
			current = current.getNext();
		}
		
		System.out.println();
	}
	
}
