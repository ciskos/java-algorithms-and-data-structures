package javaalgorithmsanddatastructures.stacks;

public class NodeStack {

	private StackNodeList list;

	public NodeStack() {
		this.list = new StackNodeList();
	}
	
	public void push(Long j) {
		list.insertFirst(j);
	}
	
	public Long pop() {
		return list.deleteFirst();
	}
	
	public Boolean isEmpty() {
		return list.isEmpty();
	}
	
	public void display() {
		System.out.print("Stack (top-->bottom): ");
		list.display();
	}
	
}
