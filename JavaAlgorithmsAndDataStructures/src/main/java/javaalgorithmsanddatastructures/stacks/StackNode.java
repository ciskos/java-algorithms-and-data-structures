package javaalgorithmsanddatastructures.stacks;

public class StackNode {

	private Long longData;
	private StackNode next;
	
	public StackNode(Long longData) {
		this.longData = longData;
	}
	
	public void display() {
		System.out.print(longData + " ");
	}

	public Long getLongData() {
		return longData;
	}

	public StackNode getNext() {
		return next;
	}

	public void setNext(StackNode next) {
		this.next = next;
	}
	
}
