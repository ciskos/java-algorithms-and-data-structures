package javaalgorithmsanddatastructures.lists;

public class FirstLastNode {

	private Long longData;
	public FirstLastNode next;
	
	public FirstLastNode(Long longData) {
		this.longData = longData;
	}

	public Long getLongData() {
		return longData;
	}

	public void display() {
		System.out.print(longData + " ");
	}
	
}
