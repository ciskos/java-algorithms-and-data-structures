package javaalgorithmsanddatastructures.lists;

public class QueueFirstLastList {

	private QueueNode first;
	private QueueNode last;
	
	public Boolean isEmpty() {
		return first == null;
	}
	
	public void insertLast(Long longData) {
		QueueNode newNode = new QueueNode(longData);
		
		if (isEmpty()) {
			first = newNode;
		} else {
			last.setNext(newNode);
		}
		
		last = newNode;
	}
	
	public Long deleteFirst() {
		Long temp = first.getLongData();
		
		if (first.getNext() == null) {
			last = null;
		}
		
		first = first.getNext();
		
		return temp;
	}
	
	public void display() {
		QueueNode current = first;
		
		while (current != null) {
			current.display();
			current = current.getNext();
		}
		
		System.out.println();
	}
	
}
