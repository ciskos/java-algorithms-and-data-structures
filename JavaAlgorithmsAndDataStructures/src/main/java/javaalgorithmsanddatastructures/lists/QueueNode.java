package javaalgorithmsanddatastructures.lists;

public class QueueNode {

	private Long longData;
	private QueueNode next;
	
	public QueueNode(Long longData) {
		this.longData = longData;
	}
	
	public void display() {
		System.out.print(longData + " ");
	}

	public Long getLongData() {
		return longData;
	}

	public QueueNode getNext() {
		return next;
	}

	public void setNext(QueueNode next) {
		this.next = next;
	}
	
}
