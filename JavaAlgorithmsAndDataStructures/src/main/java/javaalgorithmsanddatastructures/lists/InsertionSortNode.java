package javaalgorithmsanddatastructures.lists;

public class InsertionSortNode {

	private Long longData;
	private InsertionSortNode next;
	
	public InsertionSortNode(Long longData) {
		this.longData = longData;
	}

	public Long getLongData() {
		return longData;
	}

	public void setLongData(Long longData) {
		this.longData = longData;
	}

	public InsertionSortNode getNext() {
		return next;
	}

	public void setNext(InsertionSortNode next) {
		this.next = next;
	}
	
}
