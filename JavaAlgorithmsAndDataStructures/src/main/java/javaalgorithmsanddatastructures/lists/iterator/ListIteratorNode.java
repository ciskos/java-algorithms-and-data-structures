package javaalgorithmsanddatastructures.lists.iterator;

public class ListIteratorNode {

	private Long longData;
	private ListIteratorNode next;
	
	public ListIteratorNode(Long longData) {
		this.longData = longData;
	}

	public void display() {
		System.out.print(longData + " ");
	}

	public Long getLongData() {
		return longData;
	}

	public void setLongData(Long longData) {
		this.longData = longData;
	}

	public ListIteratorNode getNext() {
		return next;
	}

	public void setNext(ListIteratorNode next) {
		this.next = next;
	}
	
}
