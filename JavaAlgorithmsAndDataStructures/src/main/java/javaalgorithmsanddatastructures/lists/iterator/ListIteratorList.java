package javaalgorithmsanddatastructures.lists.iterator;

public class ListIteratorList {

	private ListIteratorNode first;
	
	public Boolean isEmpty() {
		return first == null;
	}
	
	public ListIterator getIterator() {
		return new ListIterator(this);
	}

	public void display() {
		ListIteratorNode current = first;
		
		while (current != null) {
			current.display();
			current = current.getNext();
		}
		
		System.out.println();
	}
	
	public ListIteratorNode getFirst() {
		return first;
	}

	public void setFirst(ListIteratorNode first) {
		this.first = first;
	}
	
}
