package javaalgorithmsanddatastructures.lists.iterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainListIterator {

	public static void main(String[] args) throws IOException {
		ListIteratorList list = new ListIteratorList();
		ListIterator iterator = list.getIterator();
		Long value;
		
		iterator.insertAfter(20L);
		iterator.insertAfter(40L);
		iterator.insertAfter(80L);
		iterator.insertBefore(60L);
		
		while (true) {
			System.out.print("Enter first letter of show, reset, ");
			System.out.print("next, get, before, after, delete: ");
			System.out.flush();
			
			int choice = getChar();
			switch (choice) {
				case 's':
					if (!list.isEmpty()) {
						list.display();
					} else {
						System.out.println("List is empty");
					}
					break;
					
				case 'r':
					iterator.reset();
					break;
					
				case 'n':
					if (!list.isEmpty() && !iterator.atEnd()) {
						iterator.nextNode();
					} else {
						System.out.println("Can't go to next link");
					}
					break;
					
				case 'g':
					if (!list.isEmpty()) {
						value = iterator.getCurrent().getLongData();
						System.out.println("Returned " + value);
					} else {
						System.out.println("List is empty");
					}
					break;
					
				case 'b':
					System.out.print("Enter value to insert: ");
					System.out.flush();
					
					value = getLong();
					iterator.insertBefore(value);
					break;
					
				case 'a':
					System.out.print("Enter value to insert: ");
					System.out.flush();
					
					value = getLong();
					iterator.insertAfter(value);
					break;
					
				case 'd':
					if (!list.isEmpty()) {
						value = iterator.deleteCurrent();
						System.out.println("Deleted " + value);
					} else {
						System.out.println("Can't delete");
					}
					break;
					
				default:
					System.out.println("Invalid entry");
					break;
			}
		}
	}
	
	public static String getString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		String s = br.readLine();
		
		return s;
	}

	public static char getChar() throws IOException {
		String s = getString();
		
		return s.charAt(0);
	}
	
	public static Long getLong() throws IOException {
		String s = getString();
		
		return Long.parseLong(s);
	}
	
}
