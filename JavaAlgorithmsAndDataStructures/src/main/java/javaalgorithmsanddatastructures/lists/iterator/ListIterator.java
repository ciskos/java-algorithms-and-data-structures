package javaalgorithmsanddatastructures.lists.iterator;

public class ListIterator {

	private ListIteratorNode current;
	private ListIteratorNode previous;
	private ListIteratorList list;
	
	public ListIterator(ListIteratorList list) {
		this.list = list;
		reset();
	}

	public void reset() {
		current = list.getFirst();
		previous = null;
	}
	
	public Boolean atEnd() {
		return current.getNext() == null;
	}
	
	public void nextNode() {
		previous = current;
		current = current.getNext();
	}
	
	public ListIteratorNode getCurrent() {
		return current;
	}
	
	public void insertAfter(Long longData) {
		ListIteratorNode newNode = new ListIteratorNode(longData);
		
		if (list.isEmpty()) {
			list.setFirst(newNode);
			current = newNode;
		} else {
			newNode.setNext(current.getNext());
			current.setNext(newNode);
			nextNode();
		}
	}
	
	public void insertBefore(Long longData) {
		ListIteratorNode newNode = new ListIteratorNode(longData);
		
		if (previous == null) {
			newNode.setNext(list.getFirst());
			list.setFirst(newNode);
			reset();
		} else {
			newNode.setNext(previous.getNext());
			previous.setNext(newNode);
			current = newNode;
		}
	}
	
	public Long deleteCurrent() {
		Long value = current.getLongData();
		
		if (previous == null) {
			list.setFirst(current.getNext());
			reset();
		} else {
			previous.setNext(current.getNext());
			
			if (atEnd()) {
				reset();
			} else {
				current = current.getNext();
			}
		}
		
		return value;
	}
	
}
