package javaalgorithmsanddatastructures.lists;

public class NodeQueue {

	private QueueFirstLastList list;

	public NodeQueue() {
		list = new QueueFirstLastList();
	}
	
	public Boolean isEmpty() {
		return list.isEmpty();
	}
	
	public void insert(Long j) {
		list.insertLast(j);
	}
	
	public Long remove() {
		return list.deleteFirst();
	}
	
	public void display() {
		System.out.print("Queue (front-->rear): ");
		list.display();
	}
	
}
