package javaalgorithmsanddatastructures.lists;

public class SortedList {

	private SortedListNode first;
	
	public Boolean isEmpty() {
		return first == null;
	}
	
	public void insert(Long key) {
		SortedListNode newNode = new SortedListNode(key);
		SortedListNode previous = null;
		SortedListNode current = first;
		
		while (current != null && key > current.getLongData()) {
			previous = current;
			current = current.getNext();
		}
		
		if (previous == null) {
			first = newNode;
		} else {
			previous.setNext(newNode);
		}
		
		newNode.setNext(current);
	}
	
	public SortedListNode remove() {
		SortedListNode temp = first;
		first = first.getNext();
		
		return temp;
	}
	
	public void display() {
		System.out.print("List (first-->last): ");
		
		SortedListNode current = first;
		
		while (current != null) {
			current.display();
			current = current.getNext();
		}
		
		System.out.println();
	}
	
}
