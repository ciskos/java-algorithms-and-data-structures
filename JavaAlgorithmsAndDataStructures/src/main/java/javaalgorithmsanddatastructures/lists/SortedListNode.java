package javaalgorithmsanddatastructures.lists;

public class SortedListNode {

	private Long longData;
	private SortedListNode next;
	
	public SortedListNode(Long londData) {
		this.longData = londData;
	}
	
	public void display() {
		System.out.print(longData + " ");
	}

	public Long getLongData() {
		return longData;
	}

	public void setLongData(Long longData) {
		this.longData = longData;
	}

	public SortedListNode getNext() {
		return next;
	}

	public void setNext(SortedListNode next) {
		this.next = next;
	}

}
