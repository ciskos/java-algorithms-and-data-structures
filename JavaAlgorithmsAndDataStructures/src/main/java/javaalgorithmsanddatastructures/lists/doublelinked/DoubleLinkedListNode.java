package javaalgorithmsanddatastructures.lists.doublelinked;

public class DoubleLinkedListNode {

	private Long doubleData;
	private DoubleLinkedListNode next;
	private DoubleLinkedListNode previous;
	
	public DoubleLinkedListNode(Long doubleData) {
		this.doubleData = doubleData;
	}

	public DoubleLinkedListNode getNext() {
		return next;
	}

	public void setNext(DoubleLinkedListNode next) {
		this.next = next;
	}

	public DoubleLinkedListNode getPrevious() {
		return previous;
	}

	public void setPrevious(DoubleLinkedListNode previous) {
		this.previous = previous;
	}

	public Long getDoubleData() {
		return doubleData;
	}

	public void display() {
		System.out.print(doubleData + " ");
	}

}
