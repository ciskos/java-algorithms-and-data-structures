package javaalgorithmsanddatastructures.lists.doublelinked;

public class DoubleLinkedList {

	private DoubleLinkedListNode first;
	private DoubleLinkedListNode last;
	
	public Boolean isEmpty() {
		return first == null;
	}
	
	public void insertFirst(Long doubleData) {
		DoubleLinkedListNode newNode = new DoubleLinkedListNode(doubleData);
		
		if (isEmpty()) {
			last = newNode;
		} else {
			first.setPrevious(newNode);
		}
		
		newNode.setNext(first);
		first = newNode;
	}
	
	public void insertLast(Long doubleData) {
		DoubleLinkedListNode newNode = new DoubleLinkedListNode(doubleData);
		
		if (isEmpty()) {
			first = newNode;
		} else {
			last.setNext(newNode);
			newNode.setPrevious(last);
		}
		
		last = newNode;
	}
	
	public DoubleLinkedListNode deleteFirst() {
		DoubleLinkedListNode temp = first;
		
		if (first.getNext() == null) {
			last = null;
		} else {
			first.getNext().setPrevious(null);
		}
		
		first = first.getNext();
		
		return temp;
	}
	
	public DoubleLinkedListNode deleteLast() {
		DoubleLinkedListNode temp = last;
		
		if (first.getNext() == null) {
			first = null;
		} else {
			last.getPrevious().setNext(null);
		}
		
		last = last.getPrevious();
		
		return temp;
	}
	
	public Boolean insertAfter(Long key, Long doubleData) {
		DoubleLinkedListNode current = first;
		
		while (current.getDoubleData() != key) {
			current = current.getNext();
			
			if (current == null) {
				return false;
			}
		}
		
		DoubleLinkedListNode newNode = new DoubleLinkedListNode(doubleData);
		
		if (current == last) {
			newNode.setNext(null);
			last = newNode;
		} else {
			newNode.setNext(current.getNext());
			current.getNext().setPrevious(newNode);
		}
		
		newNode.setPrevious(current);
		current.setNext(newNode);
		
		return true;
	}
	
	public DoubleLinkedListNode deleteKey(Long key) {
		DoubleLinkedListNode current = first;
		
		while (current.getDoubleData() != key) {
			current = current.getNext();
			
			if (current == null) {
				return null;
			}
		}
		
		if (current == first) {
			first = current.getNext();
		} else {
			current.getPrevious().setNext(current.getNext());
		}
		
		if (current == last) {
			last = current.getPrevious();
		} else {
			current.getNext().setPrevious(current.getPrevious());
		}
		
		return current;
	}
	
	public void displayForward() {
		System.out.print("List (first-->last): ");
		
		DoubleLinkedListNode current = first;
		
		while (current != null) {
			current.display();
			current = current.getNext();
		}
		
		System.out.println();
	}
	
	public void displayBackward() {
		System.out.print("List (last-->first): ");
		
		DoubleLinkedListNode current = last;
		
		while (current != null) {
			current.display();
			current = current.getPrevious();
		}
		
		System.out.println();
	}
	
}
