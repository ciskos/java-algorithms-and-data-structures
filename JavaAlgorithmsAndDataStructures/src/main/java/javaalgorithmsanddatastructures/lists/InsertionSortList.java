package javaalgorithmsanddatastructures.lists;

public class InsertionSortList {

	private InsertionSortNode first;
	
	public InsertionSortList(InsertionSortNode[] nodeArray) {
		for (int i = 0; i < nodeArray.length; i++) {
			insert(nodeArray[i]);
		}
	}

	public void insert(InsertionSortNode insertionSortNode) {
		InsertionSortNode previous = null;
		InsertionSortNode current = first;
		
		while (current != null && insertionSortNode.getLongData() > current.getLongData()) {
			previous = current;
			current = current.getNext();
		}
		
		if (previous == null) {
			first = insertionSortNode;
		} else {
			previous.setNext(insertionSortNode);
		}
		
		insertionSortNode.setNext(current);
	}
	
	public InsertionSortNode remove() {
		InsertionSortNode temp = first;
		first = first.getNext();
		
		return temp;
	}
	
}
