package javaalgorithmsanddatastructures.lists;

public class SimpleLinkList {

	private Node first;
	
	public Boolean isEmpty() {
		return first == null;
	}

	public void insertFirst(Integer intData, Double doubleData) {
		Node link = new Node(intData, doubleData);
		link.next = first;
		first = link;
	}
	
	public Node deleteFirst() {
		Node temp = first;
		first = first.next;
		
		return temp;
	}

	public Node find(Integer key) {
		Node current = first;
		
		while (current.getIntData() != key) {
			if (current.next == null) {
				return null;
			} else {
				current = current.next;
			}
		}
		
		return current;
	}
	
	public Node delete(Integer key) {
		Node current = first;
		Node previous = first;
		
		while (current.getIntData() != key) {
			if (current.next == null) {
				return null;
			} else {
				previous = current;
				current = current.next;
			}
		}
		
		if (current == first) {
			first = first.next;
		} else {
			previous.next = current.next;
		}
		
		return current;
	}
	
	public void display() {
		Node current = first;
		
		System.out.print("List (first-->last): ");
		while (current != null) {
			current.display();
			current = current.next;
		}
		
		System.out.println();
	}
	
}
