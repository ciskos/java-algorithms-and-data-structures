package javaalgorithmsanddatastructures.lists;

public class Node {

	private Integer intData;
	private Double doubleData;
	public Node next;
	
	public Node(Integer intData, Double doubleData) {
		this.intData = intData;
		this.doubleData = doubleData;
	}

	public Integer getIntData() {
		return intData;
	}

	public Double getDoubleData() {
		return doubleData;
	}

	public void display() {
		System.out.print("{" + intData + ", " + doubleData + "} ");
	}
	
}
