package javaalgorithmsanddatastructures.lists;

public class FirstLastList {

	private FirstLastNode first;
	private FirstLastNode last;
	
	public Boolean isEmpty() {
		return first == null;
	}

	public void insertFirst(Long longData) {
		FirstLastNode newLink = new FirstLastNode(longData);
		
		if (isEmpty()) {
			last = newLink;
		}
		
		newLink.next = first;
		first = newLink;
	}
	
	public void insertLast(Long longData) {
		FirstLastNode newLink = new FirstLastNode(longData);
		
		if (isEmpty()) {
			first = newLink;
		} else {
			last.next = newLink;
		}

		last = newLink;
	}
	
	public Long deleteFirst() {
		Long temp =first.getLongData();
		
		if (first.next == null) {
			last = null;
		}
		
		first = first.next;
		
		return temp;
	}

	public void display() {
		FirstLastNode current = first;
		
		System.out.print("List (first-->last): ");
		
		while (current != null) {
			current.display();
			current = current.next;
		}
		
		System.out.println();
	}
	
}
